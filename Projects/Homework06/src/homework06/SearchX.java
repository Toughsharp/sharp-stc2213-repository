package homework06;

public class SearchX {
    public static void main(String[] args) {
        int[] array = {2, 3, 4, 5, 6, 7, 8};
        int x = 7;
        int result = getPosition(array, x);
        System.out.println(result);

    }

    public static int getPosition(int[] array, int x) {
        for (int i = 0; i < array.length; i = i + 1) {
            if (array[i] == x) return i;
        }
        return -1;
    }


}
