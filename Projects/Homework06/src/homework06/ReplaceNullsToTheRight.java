package homework06;

import java.util.Arrays;

public class ReplaceNullsToTheRight {
    public static void main(String[] args) {

        int[] b = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        System.out.println(Arrays.toString(b));
        System.out.println(Arrays.toString(zeroToRight(b)));

    }
    static int[] zeroToRight (int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] == 0) {
                    if (array[j] != 0)  {
                        swap(array, i, j);
                    }
                }
            }
        }
        return array;
    }
    static void swap(int[] array, int index1, int index2) {
        int temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }

}
