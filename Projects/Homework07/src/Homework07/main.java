package Homework07;

public class main {

    public static void main(String[] args) {
        Human[] humans = new Human [10];
        humans[0] = new Human("Ольга", "Макеева", 22);
        humans[1] = new Human("Александр", "Савушкин", 45);
        humans[2] = new Human("Виктория", "Иванова", 30);
        humans[3] = new Human("Сергей", "Привалов", 32);
        humans[4] = new Human("Евгений", "Ковалёв", 29);
        humans[5] = new Human("Ирина", "Погодина", 53);
        humans[6] = new Human("Артур", "Макаров", 21);
        humans[7] = new Human("Владимир", "Солоха", 59);
        humans[8] = new Human("Алина", "Валеева", 26);
        humans[9] = new Human("Никита", "Савельев", 24);

        sortHumansByAge(humans);

        for(Human human: humans){
            System.out.println(human.toString());
        }

    }
    public static void sortHumansByAge(Human[] humans){
        for(int i = 0; i < humans.length; i++){
            for (int j = i + 1; j < humans.length; j++) {
                if (humans[i].getAge() > humans[j].getAge())
                    swapAge(humans, i, j);
            }
        }

    }
    static void swapAge(Human[] humans, int index1, int index2) {
        Human temp = humans[index1];
        humans[index1] = humans[index2];
        humans[index2] = temp;
    }


}
