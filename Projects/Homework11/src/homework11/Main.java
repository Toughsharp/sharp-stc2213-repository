package homework11;

public class Main {
    public static void main(String[] args) {
        Human [] humans = new Human[3];
        humans[0] = new Human("Родина", "Любовь", "Анатольевна",
                "Петрозаводск", "Державина", "28", "60", "1380 254863");
        humans[1] = new Human("Погодина", "Ирина", "Владимировна",
                "Санкт-Петербург", "Заслонова", "15", "36", "1555 536653");
        humans[2] = new Human("Прямицын", "Владимир", "Николаевич",
                "Санкт-Петербург", "Ленина", "115", "3", "5682 364785");


        for (Human human: humans){
            System.out.println(human.toString());
            System.out.println("hashcode: " + human.hashCode());
            System.out.println();
        }
        System.out.println("Сравним Родину и Погодину по паспорту");
        System.out.println(humans[0].equals(humans[1]));

    }

}
