package homework10;

public interface Condition {
    boolean isOK(int number);
}
